// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h" 
#include "RockPlatform.generated.h"

UCLASS()
class MODULE_1_STOLZ_API ARockPlatform : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARockPlatform();


public:
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals, TArray <FVector2D> & UVs, TArray <FProcMeshTangent> & Tangents, TArray <FColor> & Colors);
	// Creates the mesh display based off the input information
private:
	// FOW texture size
	static const int m_textureSize = 512;
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * mesh;
	UPROPERTY(EditAnywhere)
		class UMaterialInterface* RockMat;
	// Creates the ability to set the RockMat inside the items options in the editor

};

