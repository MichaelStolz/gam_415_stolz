// Fill out your copyright notice in the Description page of Project Settings.


#include "WoodActor.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AWoodActor::AWoodActor() 
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// add Cube to root
	UStaticMeshComponent* Cube = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WoodCube"));
	RootComponent = Cube;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));



	if (CubeAsset.Succeeded())//This sets that if the material exist to set the cube to that material
	{
		Cube->SetStaticMesh(CubeAsset.Object);
		Cube->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		Cube->SetWorldScale3D(FVector(1.f));
	}
	// Load the base material from your created material.
	{
		static ConstructorHelpers::FObjectFinder <UMaterial> asset(TEXT("Material'/Game/Materials/WooMat.WoodMat'"));
		material = asset.Object;
	}

	// Create the runtime FOW texture.
	if (!texture)
	{
		texture = UTexture2D::CreateTransient(Size, Size, PF_G8);
		texture->CompressionSettings = TextureCompressionSettings::TC_Grayscale;
		texture->SRGB = 0;
		texture->UpdateResource();
		texture->MipGenSettings = TMGS_NoMipmaps;
	}

	// Initialize array to opaque (255)
	for (int x = 0; x < Size; ++x)
		for (int y = 0; y < Size; ++y)
			m_pixelArray[y * Size + x] = 255;

	// Propagate memory's array to the texture.
	if (texture)
		texture->UpdateTextureRegions(0, 1, &Region, Size, 1, m_pixelArray);
}

